require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get proCad" do
    get :proCad
    assert_response :success
  end

  test "should get techApps" do
    get :techApps
    assert_response :success
  end

  test "should get freecad" do
    get :freecad
    assert_response :success
  end

end
