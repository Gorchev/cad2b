set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '178.62.21.246', user: 'deni', roles: %w{web app db}

set :branch, 'master'
set :rails_env, 'production'
set :deploy_to, '/var/www/k108-test.cad2b.co.uk'