set :stage, :staging

# Replace 127.0.0.1 with your server's IP address!
server '178.62.21.246', user: 'deni', roles: %w{web app db}

set :branch, 'staging'
set :rails_env, 'staging'
set :deploy_to, '/var/www/test.cad2b.co.uk'
