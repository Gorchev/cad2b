Geocoder.configure(
  lookup: :bing,
  key: Rails.application.secrets.bing_api_key,
  api_key: Rails.application.secrets.bing_api_key,
  timeout: 25,
  #cache: Rails.cache
)