Rails.application.routes.draw do

  post 'subscribtion_list/subscribe' => 'subscribtion_list#subscribe'
  
  match '/get-gstarcad-2016',  to: 'subscribtion_list#landingPage', via: 'get'
  match '/get-cadprofi-mechanical',  to: 'subscribtion_list#landingPageMech', via: 'get'
  match '/get-cadprofi-hvac-and-piping',  to: 'subscribtion_list#landingPageHVAC', via: 'get'

  #Page Errors
  get '/404' => 'errors#not_found'
  get '/500' => 'errors#internal_server_error'

  scope "(:locale)", locale: /uk|au|nz|it/ do
    resources :contact_us_messages
    resources :support_messages    
    resources :forums do
      resources :topics do
        resources :topic_answers
      end
    end
    
    #devise_for :admins
    devise_for :users

    root 'pages#index'

    match '/professional-cad',  to: 'pages#proCad', via: 'get'
    match '/technical-applications',  to: 'pages#techApps', via: 'get'
    match '/about-us',  to: 'pages#aboutUs', via: 'get'
    match '/contact-us',  to: 'contact_us_messages#new', via: 'get'
    match '/technical-applications/cadprofi-mechanical',  to: 'pages#cadprofiMechanical', via: 'get'
    match '/technical-applications/cadprofi-architectural',  to: 'pages#cadprofiArchitectural', via: 'get'
    match '/technical-applications/cadprofi-hvac-and-piping',  to: 'pages#cadprofiHVAC', via: 'get'
    match '/technical-applications/cadprofi-electrical',  to: 'pages#cadprofiElectrical', via: 'get'
    match '/privacy-policy',  to: 'pages#privacyPolicy', via: 'get'
    match '/site-map',  to: 'pages#siteMap', via: 'get'
    match '/users/downloads',  to: 'pages#downloadArea', via: 'get'
    match '/users/dashboard',  to: 'pages#userDashboard', via: 'get'
    match '/posts',  to: 'posts#index', via: 'get'
    match '/administrator/posts/new',  to: 'posts#new', via: 'get'
    match '/posts/create',  to: 'posts#create', via: 'post'
    match '/administrator/dashboard',  to: 'admin_dashboard#index', via: 'get'
    match '/administrator/posts',  to: 'admin_dashboard#posts', via: 'get'
    match '/administrator/users',  to: 'admin_dashboard#users', via: 'get'
    match '/administrator/forums',  to: 'admin_dashboard#forums', via: 'get'
    match '/administrator/posts/edit',  to: 'posts#edit', via: 'get'
    match '/administrator/posts/update',  to: 'posts#update', via: 'post'
    match '/posts/:id',  to: 'posts#show', via: 'get', as: :slug
    match '/administrator/posts/destroy',  to: 'posts#destroy', via: 'delete'
    match '/administrator/users/:id',  to: 'users#destroy', via: 'delete', as: :administrator_user_destroy
    match '/administrator/users/:id',  to: 'users#edit', via: 'get', as: :administrator_user_edit
    match '/administrator/users/:id',  to: 'users#update', via: 'patch', as: :administrator_user_update
    match '/users/downloads/cad-profi',  to: 'download#downloadCADprofi', via: 'get'
    match '/users/downloads/gstar-cad-64',  to: 'download#downloadGCAD64', via: 'get'
    match '/users/downloads/gstar-cad-32',  to: 'download#downloadGCAD32', via: 'get'
    match '/users/downloads/gstar-cad-pdf',  to: 'download#downloadGCADPDF', via: 'get'

    match '/categories',  to: 'categories#index', via: 'get'
    match '/categories/new',  to: 'categories#new', via: 'get'
    match '/categories/post',  to: 'categories#create', via: 'post'
  end

end
