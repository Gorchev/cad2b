class ForumsController < ApplicationController
  def index
  	@forums = Forum.all
  end

  def new
  	@forum = Forum.new
    render :layout => "admin"
  end

  def create
  	@forum = Forum.new(forum_params)
  	if @forum.save
      redirect_to action: :index
    else
      render('new')
    end
  end

  def edit
  	@forum = Forum.find(params[:id])
    render :layout => "admin"
  end

  def update
    @forum = Forum.find(params[:id])
  	if @forum.update_attributes(forum_params)
  	  redirect_to(controller: "forums", action: "index")
  	else
  	  render('new')
  	end
  end

  def destroy
    @forum = Forum.find(params[:id])
    if @forum.destroy
      redirect_to(controller: "admin_dashboard", action: "forums")
    else
      render('new')
    end
  end

  private
    def forum_params
      params.require(:forum).permit(:title, :description)
    end
end
