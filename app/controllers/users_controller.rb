class UsersController < ApplicationController
  def edit
    @roles = Role.all
    @user = User.find(params[:id])
  end

  def update
    @roles = Role.all
    @user = User.find(params[:id])
    if @user.update_attributes(users_params)
      redirect_to administrator_users_path
    else
      render('edit');
  	end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      redirect_to administrator_users_path
    end
  end

  private
    def users_params
      params.require(:user).permit(:name, :surname, :avatar, :country, :city, :postcode ,:phone, :role_id)
    end
end