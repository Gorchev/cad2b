class AdminDashboardController < ApplicationController
  
  before_filter :authenticate_user!
  before_filter :authenticate_user_role

  layout "admin"

  def index
  	@posts = Post.all.order('created_at DESC')
  	@users = User.where(role_id: 1).order('created_at DESC')
    @topics = Topic.all.order('created_at DESC')
  end

  def posts
  	@posts = Post.all.order('created_at DESC')
  end

  def users
  	@users = User.all.order('created_at DESC')
    attributes_to_include = %w(id name surname email country phone created_at)
    respond_to do |format|
      format.html
      format.csv { send_data @users.to_csv(attributes_to_include), filename: "cad2b-users-#{Time.now.strftime("%d%m%Y%H%M%S")}.csv" }
      format.xls {
        response.header['Content-Disposition'] = "attachment; filename=\"cad2b-users-#{Time.now.strftime("%d%m%Y%H%M%S")}.xls\""
      }
    end
  end

  def forums
    @forums = Forum.all.order('created_at DESC')
  end

  private
    def authenticate_user_role
      if current_user.role_id != 4 #Admin
        redirect_to users_dashboard_path
      end
    end
  
end
