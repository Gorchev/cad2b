class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale
  before_filter :configure_permited_paramteters, if: :devise_controller?

  def configure_permited_paramteters
     devise_parameter_sanitizer.for(:sign_up) << [:name, :surname, :avatar, :country, :city, :postcode ,:phone, :subscribe]
     devise_parameter_sanitizer.for(:account_update) << [:name, :surname, :avatar, :country, :city, :postcode ,:phone]
  end

  def after_sign_in_path_for(resource)
    if resource.role_id == 4
      administrator_dashboard_path
    else
      users_dashboard_path
    end
  end

  private 

    def location
      if params[:location].blank?
        if Rails.env.test? || Rails.env.development?
          @location ||= Geocoder.search("137.147.5.176").first
        else
          @location ||= request.location
        end
      else
        params[:location].each {|l| l = l.to_i } if params[:location].is_a? Array
        @location ||= Geocoder.search(params[:location]).first
        @location
      end
    end

    def set_locale
      if params[:locale].blank?
        session[:referrer]=url_for(params)
        if URI(session[:referrer]).path.include?("technical-applications") == true
          redirect_to("/#{get_the_country_code}/technical-applications")
        elsif URI(session[:referrer]).path.include?("get-") == false
          redirect_to("/#{get_the_country_code}")
        end
      else
        I18n.locale = params[:locale]
      end
    end

    def get_the_country_code
      @loc = location.country_code
      if @loc == "AU"
        I18n.locale = "au"
      elsif @loc == "NZ"
        I18n.locale = "nz"
      elsif @loc == "IT"
        I18n.locale = "it"
      else 
        I18n.locale = "uk"  
      end
    end

    def self.default_url_options(options = {})
      {locale: I18n.locale}
    end

end