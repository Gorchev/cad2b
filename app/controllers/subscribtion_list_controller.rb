class SubscribtionListController < ApplicationController

  after_filter :flash_to_headers

  def index
  end

  def subscribe
    #Email address
    @email = params[:email][:address]
    @name = params[:email][:firstname]
    @surname = params[:email][:lastname]
  	@list_id = Rails.application.secrets.mailchimp_list_id
    gb = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)

    #Generate password
    @generated_password = Devise.friendly_token.first(8)
    
    #Create a user, if it doesn't exist
    if User.exists?({email: @email})
      flash[:error] = "The email already exist"
    else
      @user = User.create!(:email => @email, :password => @generated_password, :name => @name, :surname => @surname)

      #Send a message with the email address and the password generated
      WelcomeSubscribeMailer.your_password(@email, @name, @generated_password).deliver

      gb.lists(@list_id).members.create(body: {
      	email_address: params[:email][:address], 
      	status: "subscribed", 
      	merge_fields: {
      		FNAME: params[:email][:firstname], 
      		LNAME: params[:email][:lastname]
      	}
      })
    end

  end

  def landingPage
    render layout: "landing"
  end

  def landingPageMech
    render layout: "landing"
  end

  def landingPageHVAC
    render layout: "landing"
  end

  def flash_to_headers
    return unless request.xhr?
    response.headers['X-Message'] = flash[:error] unless flash[:error].blank?
    flash.discard
  end
end
