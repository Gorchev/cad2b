class PostsController < ApplicationController

 before_filter :find_page, only: [:show, :edit, :update]
 
 def index
  @posts = Post.where(is_draft: false).order("created_at DESC")
  @last_published_post = Post.where(is_draft: false).last
 end

 def show
 end

 def new
  @post = current_user.posts.build
 end

 def create
  @post = current_user.posts.build(post_params)
  if @post.save
   redirect_to(action: 'show', id:@post.slug)
  else 
   render("new")
  end
 end

 def edit
  render :layout => 'admin'
 end

 def update
  if @post.update_attributes(post_params)
    redirect_to(action: 'show', id:@post.slug)
  else
    render('new')
  end
 end

 def destroy
  @post = Post.find(params[:id]).destroy
  redirect_to(administrator_posts_path)
 end

 private
  def post_params
   params.require(:post).permit(:title, :content, :title_it, :content_it, :featured_image, :category_id, :is_draft)
  end

  def find_page
   @post = Post.friendly.find(params[:id])
  end
end
