class ContactUsMessagesController < ApplicationController
  def new
  	@message = ContactUsMessage.new
  end

  def create
  	@message = ContactUsMessage.new(message_params)
  	if @message.valid?
  		ContactUsMailer.send_mailer(@message).deliver
  		respond_to do |format|
          format.js
  		end
  	end
  end

  private
    def message_params
  	  params.require(:contact_us_message).permit(:email, :subject, :message)
    end
end
