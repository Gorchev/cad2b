class PagesController < ApplicationController
  
  before_filter :authenticate_user!, :only => [:downloadArea, :userDashboard]
  before_filter :support_message, :only => [:proCad, :cadprofiMechanical, :cadprofiArchitectural, :cadprofiElectrical, :cadprofiHVAC]

  def index
    @posts = Post.where(is_draft: false).order("created_at DESC")
  end

  def proCad
  	@posts = Post.where(is_draft: false).order("created_at DESC")
  end

  def techApps
    @posts = Post.where(is_draft: false).order("created_at DESC")
  end

  def cadprofiMechanical
  end

  def cadprofiArchitectural
  end

  def cadprofiElectrical
  end

  def cadprofiHVAC
  end

  def downloadArea
  end

  def userDashboard
    @topics = Topic.all.order("created_at DESC")
    @topic_answers = TopicAnswer.all.order("created_at DESC")
  end

  def freecad
  end 

  def privacyPolicy
  end 

  def siteMap
  end 

  private
    def support_message
      @message = SupportMessage.new
    end

end
