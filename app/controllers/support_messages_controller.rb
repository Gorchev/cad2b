class SupportMessagesController < ApplicationController
  def new
  	@message = SupportMessage.new
  end

  def create
    @message = SupportMessage.new(message_params)
	if @message.valid?
		SupportMailer.message_me(@message).deliver
		respond_to do |format|
			format.html {render :new}
			format.js
		end
	end
  end

  private

  def message_params
    params.require(:support_message).permit(:name, :email, :subject, :message)
  end
end
