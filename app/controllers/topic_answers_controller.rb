class TopicAnswersController < ApplicationController
  def show
  end
  
  def edit
    @answer = TopicAnswer.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def update
    @answer = TopicAnswer.find(params[:id])
    if @answer.update_attributes(topic_answers_params)
      respond_to do |format|
        format.js
      end
    else
      redirect_to("index")
    end
  end

  def new
  	respond_to do |format|
      format.js
  	end
  end

  def create
  	@topic_answers = current_user.topic_answers.build(topic_answers_params)
  	if @topic_answers.save
  	  respond_to do |format|
        format.js
  	  end
    else
  	  redirect_to("index")
    end
  end

  private
  	def topic_answers_params
  	  params.require(:topic_answer).permit(:content, :user_id, :topic_id)
  	end
end
