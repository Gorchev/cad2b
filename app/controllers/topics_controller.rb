class TopicsController < ApplicationController
  def index
  end

  def show
    @topic = Topic.find(params[:id])
  end

  def new
    @forums = Forum.all()
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    if user_signed_in?
    	@topic = current_user.topics.build(topic_params)
    elsif admin_signed_in?
      @topic = current_admin.topics.build(topic_params)
    end
  	if @topic.save 
      respond_to do |format|
        format.js
      end
  	else
  	  redirect_to('new')
  	end
  end

  def edit
  	@topic = Topic.find(params[:id])
  end

  def update
  	@topic = Topic.find(params[:id])
  	if @topic.update_attributes(topic_params)
  	  render('index')
  	else
  	  render('new')
  	end
  end

  def delete
    @topic = Topic.find(params[:id])
  end

  def destroy
    @topic = Topic.find(params[:id])
    if @topic.destroy
      redirect_to forums_path
    else
      render("show")
    end
  end

  private
  	def topic_params
  		params.require(:topic).permit(:title, :content, :user_id, :forum_id)
  	end
end
