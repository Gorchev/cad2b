class DownloadController < ApplicationController
  def downloadCADprofi
    send_file(
      "#{Rails.root}/public/downloads/cadprofi_cad2b.zip",
      filename: "cadprofi_cad2b.zip",
      type: "application/zip"
    )
  end

  def downloadGCAD64
    send_file(
      "#{Rails.root}/public/downloads/GstarCAD2016_x64.zip",
      filename: "GstarCAD2016_x64.zip",
      type: "application/zip"
    )
  end

  def downloadGCAD32
    send_file(
      "#{Rails.root}/public/downloads/GstarCAD2016_x86.zip",
      filename: "GstarCAD2016_x86.zip",
      type: "application/zip"
    )
  end

  def downloadGCADPDF
    send_file(
      "#{Rails.root}/public/downloads/#{params[:filename]}.pdf",
      filename: "#{params[:filename]}.pdf",
      type: "application/pdf"
    )
  end
end
