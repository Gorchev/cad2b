class WelcomeSubscribeMailer < ActionMailer::Base

  def your_password(email, name, password)
    @time = Time.now.strftime("%d %B %Y")
  	@email = email
  	@generated_password = password
  	@name = name
  	mail from: "support@cad2b.co.uk", to: @email, subject: 'Your credentials for CAD2B'
  end

end
