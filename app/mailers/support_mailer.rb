class SupportMailer < ApplicationMailer
  def message_me(support_message)
    @msg = support_message
    mail from: "#{@msg.name} <#{@msg.email}>", to: "support@cad2b.co.uk", subject: @msg.subject, body: @msg.message
  end
end
