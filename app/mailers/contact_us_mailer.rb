class ContactUsMailer < ApplicationMailer
  def send_mailer(contact_us_message)
  	@msg = contact_us_message
    mail from: "#{@msg.email}", to: "support@cad2b.co.uk", subject: @msg.subject, body: @msg.message
  end
end
