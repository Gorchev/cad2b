class WelcomeUserMailer < ActionMailer::Base
  def welcome(user)
    @user = user
    mail from: "support@cad2b.co.uk", to: @user.email, subject: "Welcome to CAD2B!"
  end
end
