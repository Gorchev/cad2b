class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts
  has_many :topics
  has_many :topic_answers
  belongs_to :role

  #Make fields required
  validates_presence_of [:name, :surname, :email]

  attr_accessor :subscribe
  attr_accessor :iscadprofipage

  #Send the Welcome email after new registration
  after_create :congrats_email if :iscadprofipage == false

  #Subscribe User to Mailchimp
  after_create :subscribe_to_list 

  #Set default "Registered" role to the new users
  before_create :set_default_role

  #Titelize some of the fields in the user tables, on save and create
  before_create :titleize_user_columns
  before_save :titleize_user_columns

  #User avatar
  has_attached_file :avatar, styles: { medium: "600x600>", thumb: "100x100>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  #Set the CSV and Xls methods for the Users
  def self.to_csv(attributes = column_names, options = {})
    CSV.generate(options) do |csv|
      csv.add_row attributes
      all.each do |user|
        values = user.attributes.slice(*attributes).values
        csv.add_row values
      end
    end
  end

  def congrats_email
    WelcomeUserMailer.welcome(self).deliver
  end

  def subscribe_to_list
    #If the subcribe box is checked, subscribe the User
    if self.subscribe == "1"
      @list_id = Rails.application.secrets.mailchimp_list_id
      gb = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)

      gb.lists(@list_id).members.create(body: {
        email_address: self.email, 
        status: "subscribed", 
        merge_fields: {
          FNAME: self.name, 
          LNAME: self.surname
        }
      })
    end
  end

  private 
    def set_default_role
      self.role ||= Role.find_by_name('registered')
    end

    def titleize_user_columns
      self.name = self.name.titleize
      self.surname = self.surname.titleize
      self.country = self.country.titleize if self.country.blank? == false
      self.city = self.city.titleize  if self.city.blank? == false
    end

end
