class Post < ActiveRecord::Base
  
  belongs_to :user
  belongs_to :cateogry

  has_attached_file :featured_image, styles: { medium: "1366x768>", thumb: "100x100>" }
  validates_attachment_content_type :featured_image, content_type: /\Aimage\/.*\Z/

  extend FriendlyId
  friendly_id :title, use: :slugged

end
