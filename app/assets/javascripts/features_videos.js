var channelID = "UC-npS4zMMV9EFdR-PwTLX2g"; 
var pid;
var pageTokenValue;
var pathArray = window.location.pathname.split( '/' );
var partial = pathArray[3];
var partial2 = pathArray[2];

$(document).ready(function(){
	var $popupWrapper = $(".pop_up_wrapper");
	var $videoPlaceholder = $(".pop_up_panel .video");
	var video1 = '<iframe width="560" height="315" src="https://www.youtube.com/embed/XHBoU7BluQs" frameborder="0" allowfullscreen></iframe>'

	$(".pop_up_panel .close").on("click", function(e){
		e.preventDefault();
		$popupWrapper.fadeOut();
		$videoPlaceholder.find("iframe").attr("src","");
		$popupWrapper.find(".description h3").text("");
		$popupWrapper.find(".description p").text("");
	});

	//Set the proper playlistId to show the according videos
	if (partial == "cadprofi-electrical"){
		pid = "PLPnjoP0SfnotfVoZW-EGxZNTm6737yMa2";
	} else if (partial == "cadprofi-mechanical"){
		pid = "PLPnjoP0SfnosNgjuaxbG04mNG1m_p9Rc7";
	} else if (partial == "cadprofi-architectural"){
		pid = "PLPnjoP0SfnosTpJdv-49k8QIMmNkSgr2q";
	} else if (partial == "cadprofi-hvac-and-piping"){
		pid = "PLPnjoP0SfnovEfuJvb5Jf-CDhwDvw_dsi";
	} else if (partial2 == "professional-cad"){
		pid = "PLPnjoP0Sfnovc5tj-nyuB9KrK8ufdVnVz";
	}

	//Features on click load a pop_up with video
	$("#features ul li a, .new_features ul li a").on("click", function(e){
		var thisName = $(this).parent().attr("data-name");

 		//Get the data-id from the featured video
		var thisId = $(this).parent().attr("data-id");
		
		//Check whether is 1 or 2 and apply the Page Token accordingly.
		//The Page Token allows to list all the videos from
		//next / prev page from youtube api (GstarCAD page only)
		if (partial2 == "professional-cad" && thisId == 2){
			pageTokenValue = "CDIQAA";
		} else if (partial2 == "professional-cad" && thisId == null){
			pageTokenValue = "CDIQAQ";
		}
		
		e.preventDefault();
		$popupWrapper.fadeIn();

		$.get(
		      "https://www.googleapis.com/youtube/v3/channels", {
		      part: "contentDetails",
		      id: channelID,
		      key: "AIzaSyAMNe1KDhTG7HE_GneDFw4Ek8CGbcfY1oI"},
		      function(data){
		        $.each(data.items, function(i, item){
		        	console.log(pid);
		          getVids(pid);
		        })
		      }
		  );

		  function getVids(pid){
		    $.get(
		        "https://www.googleapis.com/youtube/v3/playlistItems", {
		        part: "snippet",
		        maxResults: 50,
		        playlistId: pid,
		        pageToken: pageTokenValue,
		        key: "AIzaSyAMNe1KDhTG7HE_GneDFw4Ek8CGbcfY1oI"},
		        function(data){
		          var output;
		          console.log(data);
		          $.each(data.items, function(i, item){
		            videoTitle = item.snippet.title;
		            //console.log(videoTitle);
		            if (videoTitle == thisName){
			            videoDesc = item.snippet.description;
			            videoId = item.snippet.resourceId.videoId;
			            $(".pop_up_panel .video iframe").attr("src", "https://www.youtube.com/embed/"+videoId);
			            $(".pop_up_panel .description h3").append(videoTitle);
			            $(".pop_up_panel .description p").append(videoDesc);
		            }
		          })
		        }
		    );
		  }
	})

		/*$.get(
		      "https://www.googleapis.com/youtube/v3/channels", {
		      part: "contentDetails",
		      id: channelID,
		      key: "AIzaSyAMNe1KDhTG7HE_GneDFw4Ek8CGbcfY1oI"},
		      function(data){
		        $.each(data.items, function(i, item){
		          console.log(item);
		          pid = item.contentDetails.relatedPlaylists.uploads ;
		          getVids(pid);
		        })
		      }
		  );

		  function getVids(pid){
		    $.get(
		        "https://www.googleapis.com/youtube/v3/playlistItems", {
		        part: "snippet",
		        playlistId: pid,
		        key: "AIzaSyAMNe1KDhTG7HE_GneDFw4Ek8CGbcfY1oI"},
		        function(data){
		          var output;
		          $.each(data.items, function(i, item){
		            console.log(item);
		            videoTitle = item.snippet.title;
		            if (videoTitle == "GstarCAD 2011: Single-Line Text"){
			            videoDesc = item.snippet.description;
			            videoId = item.snippet.resourceId.videoId;
			            $(".pop_up_panel .video iframe").attr("src", "https://www.youtube.com/embed/"+videoId);
			            $(".pop_up_panel .description h3").append(videoTitle);
			            $(".pop_up_panel .description p").append(videoDesc);
		            }
		          })
		        }
		    );
		  }*/

});