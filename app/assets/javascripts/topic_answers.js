// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function(){

	//Create and send the answer to the server DB
	$("#topicWall").on("click", ".post", function(){
		$.ajax({
			type: "POST",
			url: currentTopicId+"/topic_answers",
			data: {topic_answer: {content: $('#topicWall .content.contenteditable').html(),forum_id: currentForumId, topic_id: currentTopicId}},
			success: function(){
				console.log("Success");
				$("#topicWall .action").append("<span class='outcome success'>Posted</span>")
			},
			error: function(){
				console.log("Error");
			}
		});
	});

	//Update and send the answer to the server DB
	$("#topicWall").on("click", "#answers .answer_list i.save", function(){
		var thisEl = $(this);
		$.ajax({
			type: "PUT",
			url: currentTopicId+"/topic_answers/"+currentAnswerId,
			data: {topic_answer: {content: $('#topicWall .content.contenteditable').html(),forum_id: currentForumId, topic_id: currentTopicId}},
			success: function(){
				thisEl.parent().addClass("success");
				thisEl.parent().append("<span class='answer_outcome succeeded'>Updated successfully!</span>")
			},
			error: function(){
				thisEl.parent().addClass("error");
				thisEl.parent().append("<span class='answer_outcome fail'>There was an error saving this answer... Please refersh the page.</span>")
			}
		});
	});

	//////////////////////////
	//Show WYSIWYG
	//////////////////////////

	//Hide the wysiwyg buttons
	function hideButtons(){
		$("#topicWall #wysiwygEditor button").each(function(i){
			var thisButton = $(this);
			setTimeout(function(){
				thisButton.removeClass("active");
			}, 50*i);
		});	
	}
	$("#topicWall").on("mouseup", ".content.contenteditable", function(){

		//Get selection
		var selection = window.getSelection();

		//Get the range of the selection
		var selectionRange = selection.getRangeAt(0);

		//Show the wysiwyg buttons 
		function showButtons(){
			$("#topicWall #wysiwygEditor button").each(function(i){
				var thisButton = $(this);
				setTimeout(function(){
					thisButton.addClass("active");
				}, 50*i);
			});	
		}

		//Show the wysiwyg buttons if the range between the first letter
		//and the cursor pointer is more than 1
		if(selectionRange.endOffset - selectionRange.startOffset > 0){
			showButtons();
		} else {
			hideButtons();
		}

		//Hide the buttons if you start typing
		window.onkeydown = function(){
			hideButtons();	
		};
		
	});

	//On paste, strip all html tags
	$("#topicWall").on("paste", ".contenteditable", function(){
		setTimeout(function(){
			var currentContent = $(".contenteditable").html();
			var div = document.createElement("div");
			div.innerHTML = currentContent;
			$(".contenteditable").html(div.innerText || div.textContent);
		}, 10)
	});

	//On focus out from the contenteditable, remove buttons and save button
	$("#topicWall").on("click", "i.close", function(){
		//Make content - not contenteditable
		$(".contenteditable").attr("contenteditable", false);
		//Add the edit button
		$(".contenteditable").parent().find(".edit").removeClass("hide");
		//Hide the save button
		$(".contenteditable").parent().find(".save").removeClass("show");
		//Hide the close button
		$(".contenteditable").parent().find(".close").removeClass("show");
		//Hide WYSIWYG buttons
		hideButtons();
		setTimeout(function(){
			$("#wysiwygEditor").remove();
		}, 300)
		//Remove class contenteditabe
		$(".contenteditable").removeClass("contenteditable");
	});
	$("#topicWall").on("click", "i.save", function(){
		//Make content - not contenteditable
		$(".contenteditable").attr("contenteditable", false);
		//Add the edit button
		$(".contenteditable").parent().find(".edit").removeClass("hide");
		//Hide the save button
		$(".contenteditable").parent().find(".save").removeClass("show");
		//Hide the close button
		$(".contenteditable").parent().find(".close").removeClass("show");
		//Hide WYSIWYG buttons
		hideButtons();
		setTimeout(function(){
			$("#wysiwygEditor").remove();
		}, 300)
		//Remove class contenteditabe
		$(".contenteditable").removeClass("contenteditable");
	});
});