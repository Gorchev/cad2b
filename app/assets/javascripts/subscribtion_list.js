$(document).ready(function () {
	//Validation for the "Quote contact form" on the 
	//product pages
	$(".home #notification .window .send").on("click", function(){
		if(validateFirstName() & validateLastName() & validateEmail()){			
			//window.location.href = '/downloads/GstarCAD_2016_Complete_Guide.zip';
			return true;
		} else {
			return false;
		}
	});

	//Validate the FirstName
	function validateFirstName(){
		if($("#mailName").val().length < 3){
			console.log("asd");
			$("#mailName").addClass("error");
			$("#mailName").attr("placeholder", "Please type at least 3 characters");
			return false
		} else {
			$("#mailName").removeClass("error");
			return true
		}
	}

	//Validate the Last Name
	function validateLastName(){
		if($("#mailSurname").val().length < 3){
			console.log("asd");
			$("#mailSurname").addClass("error");
			$("#mailSurname").attr("placeholder", "Please type at least 3 characters");
			return false
		} else {
			$("#mailSurname").removeClass("error");
			return true
		}
	}

	//Validate the Email
	function validateEmail(){
		var a = $("#mailEmail").val();
		var regexp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

		if(regexp.test(a)){
			$("#mailEmail").removeClass("error");
			return true
		} else {
			$("#mailEmail").attr("placeholder", "Please enter a valid email address");
			$("#mailEmail").addClass("error");
			return false
		}
	}
	
});