$(window).load(function(){

	/* Languages */
	$(".language .btn").on("click", function(){
		console.log("hey");
		if ($(this).hasClass("italian")){
			$("#newPost .content_post").fadeOut();
			$("#newPost .content_post_it").fadeIn();
			$(".btn.english").removeClass("current blue");
			$(this).addClass("current blue");
		} else {
			$("#newPost .content_post_it").fadeOut();
			$("#newPost .content_post").fadeIn();
			$(".btn.italian").removeClass("current blue");
			$(".btn.english").addClass("current blue");
		}
	});

	//Make the height of the TinyMCE editor
	//as high as the text inside
	var editorBodyHeight = $("#editor_ifr, #editor-1_ifr").contents().find(".mce-content-body").height();
	$("#editor_ifr, #editor-1_ifr").height(editorBodyHeight + 10);
	$("#editor_ifr, #editor-1_ifr").contents().find(".mce-content-body").keydown(function(){
		editorBodyHeight = $("#editor_ifr, #editor-1_ifr").contents().find(".mce-content-body").height();
		$("#editor_ifr, #editor-1_ifr").height(editorBodyHeight + 20);
		$(".contents").height(editorBodyHeight);
	});

	//Set the height of the contents container
	//to be as high as the content inside
	$(".contents").height(editorBodyHeight);

	//Add some styling to the paragraphs in the 
	//Posts editor
	$("#editor_ifr, #editor-1_ifr").contents().find(".mce-content-body p").css({
		'line-height':"1.5em !important",
		'margin-bottom':"20px",
		'color':'#666'
	});

	$("#editor_ifr, #editor-1_ifr").contents().find(".mce-content-body ul").css({
		'margin-bottom':"20px !important",
		'list-style':'disc !important',
		'padding-left':'20px !important'
	});

	//Initialize Masonry
	$('#dashboardWall .grid').masonry({
	  // set itemSelector so .grid-sizer is not used in layout
	  itemSelector: '.box',
	  // use element for option
	  columnWidth: '.box',
	  gutter: 30,
	  isFitWidth: true
	})


	/* Google Analytics API Dashboard */
	gapi.analytics.ready(function() {

	  /**
	   * Authorize the user immediately if the user has already granted access.
	   * If no access has been created, render an authorize button inside the
	   * element with the ID "embed-api-auth-container". sRVik0bD8PZivm4g1H5hbzPk
	   */
	  gapi.analytics.auth.authorize({
	    container: 'embed-api-auth-container',
	    clientid: '1093491041436-1ds498d9khrpnatngpdqfmtte76sldh1.apps.googleusercontent.com'
	  });


	  /**
	   * Create a new ViewSelector instance to be rendered inside of an
	   * element with the id "view-selector-container".
	   */
	  var viewSelector = new gapi.analytics.ViewSelector({
	    container: 'view-selector-container'
	  });

	  // Render the view selector to the page.
	  viewSelector.execute();


	  /**
	   * Create a new DataChart instance with the given query parameters
	   * and Google chart options. It will be rendered inside an element
	   * with the id "chart-container".
	   */
	  var dataChart = new gapi.analytics.googleCharts.DataChart({
	    query: {
	      metrics: 'ga:sessions',
	      dimensions: 'ga:date',
	      'start-date': '30daysAgo',
	      'end-date': 'yesterday'
	    },
	    chart: {
	      container: 'chart-container',
	      type: 'LINE',
	      options: {
	        width: '100%'
	      }
	    }
	  });


	  /**
	   * Render the dataChart on the page whenever a new view is selected.
	   */
	  viewSelector.on('change', function(ids) {
	    dataChart.set({query: {ids: ids}}).execute();
	  });

	});

});