$(document).ready(function(){
	//Remove the error class from the fields when you
	//start typing
	$("#new_support_message input, #new_support_message textarea").keydown(function(){
		console.log("asd");
		$(this).removeClass("error");
	});

	$("#new_support_message").submit(function(){
		$.post(
			$(this).attr('action') + '.js',
			$(this).serialize(),
			null,
			"script"
		);
		return false;
	});
});