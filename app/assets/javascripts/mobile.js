$(document).ready(function(){

	//Trigger mobile menu
	$(".mobile_trigger").on("click", function(){
		$(this).toggleClass("clicked");
		$(".nav").toggleClass("active");

		//Add class active to each list
		$(".nav > ul > li").each(function(i){
			var thisLi = $(this);
			setTimeout(function(){
				thisLi.toggleClass("active");
			}, 100*(i+1));
		})
	
	});

	//
	// Pro CAD Page
	//

	//Expand the feature to show all features 
	$(".procad .feature h3").on("click", function(){
		var thisItem = $(this);
		//Hide all features
		$(".procad .feature ul").slideUp();

		$(".procad .feature ul li").each(function(i){
			var thisLi = $(this);
			thisLi.removeClass("active");
		});

		//Show or toggle show the current feature
		if (thisItem.next().is(":hidden")){
			thisItem.next().slideDown();

			//Show every item one by one
			thisItem.parent().find("ul li").each(function(i){
				var thisLi = $(this);
				setTimeout(function(){
					thisLi.addClass("active");
				}, 100*(i+1));
			});
		}
	});
});