// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function(){
	$(".forum_container").on("click", "#post_topic", function(){
		var selectedForum = $("#Forums").val();
		var topicTitle = $(".contenteditable .the_title").text();
		var topicContent = $(".contenteditable .the_content").html();
		$.ajax({
			type: 'POST',
			url: 'forums/'+selectedForum+'/topics',
			data: {topic: {title: topicTitle, content: topicContent, forum_id: selectedForum}},
			success: function(){
				$("#forum"+selectedForum+" ul").prepend("<li class='just_added_topic' style='display:none;'><h4><a href='forums/"+selectedForum+"/topics/"+newTopicId+"'>"+topicTitle.substring(0, 40)+"</a></h4><span>"+topicContent.substring(0, 40)+"...</span><span class='by_author'>by "+currentUserName+" "+currentUserSurname+"</span><span class='answers'>0 answers</span></li>");
				
				//Show the new post				
				setTimeout(function(){
					$(".just_added_topic").fadeIn();
				}, 200);

				//Append a successfull message
				$(".forum_container .action").append("<span class='outcome success'>Posted</span>")

				//Disable Button
				$(".forum_container .action .post_topic").attr("disabled", true)

				//Add a class disabled to the button
				$(".forum_container .action .post_topic").addClass("disabled")
			},
			error: function(){
				console.log("error");
			}
		});
	});

	//Update and send the title to the server DB
	$("#topicWall").on("click", ".title_area i.title_save", function(){
		var thisEl = $(this);
		$.ajax({
			type: "PUT",
			url: currentTopicId,
			data: {topic: {title: $('#topicWall .main_title').html(),forum_id: currentForumId}},
			success: function(){
				thisEl.parent().addClass("success");
				thisEl.parent().append("<span class='answer_outcome succeeded'>Updated successfully!</span>")
			},
			error: function(){
				thisEl.parent().addClass("error");
				thisEl.parent().append("<span class='answer_outcome fail'>Posted</span>")
			}
		});
	});

	//Update and send the answer to the server DB
	$("#topicWall").on("click", ".initial i.content_save", function(){
		var thisEl = $(this);
		$.ajax({
			type: "PUT",
			url: currentTopicId,
			data: {topic: {content: $('#topicWall .content').html(),forum_id: currentForumId}},
			success: function(){
				thisEl.parent().addClass("success");
				thisEl.parent().append("<span class='answer_outcome succeeded'>Updated successfully!</span>")
			},
			error: function(){
				thisEl.parent().addClass("error");
				thisEl.parent().append("<span class='answer_outcome fail'>Posted</span>")
			}
		});
	});
	//On paste, strip all html tags
	$(".forum_container").on("paste", ".the_content", function(){
		setTimeout(function(){
			var currentContent = $(".the_content").html();
			var div = document.createElement("div");
			div.innerHTML = currentContent;
			$(".the_content").html(div.innerText || div.textContent);
		}, 10)
	});
});
