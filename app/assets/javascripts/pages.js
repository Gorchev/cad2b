$(document).ready(function(){

	//Accordion functionality for the 'Product Comparison'
	//section
	$("#productComparison .header").click(function(){
	    if(false == $(this).next().is(':visible')) {
	        $('#productComparison .expand_collapse').slideUp(300);
	    }
	    $(this).next().slideToggle(300);
	});
	 
	$('#productComparison .table-info:eq(0) .expand_collapse').show();

	//Quote pop up
	$("#quote h4").on("click", function(){
		$(".hidden_area").slideToggle();
		$("#quote h4").toggleClass("expanded");
	});

	//Stick the submenu on the Products pages
	$(window).scroll(function(){
		var offsetTop = $(window).scrollTop();
		if(offsetTop >= 84){
			$("#submenu").addClass("stick");
		} else {
			$("#submenu").removeClass("stick");
		}
	});

	//Submenu - Mobile selection on change go to selection
	$("#submenu select").on("change", function(){
		window.location.hash = $(this).find("option:selected").attr("value");
	});

	//Call the function "onScroll" here
	$(document).on("scroll", onScroll);

	//User Downloads - Product System Rquirements
	$("#userDashboard .apps_download .left a").on("click", function(e){
		e.preventDefault();
		$(this).parents("li").find(".sys_req").slideToggle();
	});

	//Switch the download bit options of the GstarCAD
	$("#bitVersion").change(function(){
		var currentBit = $("#bitVersion option:selected").attr("name");
		$(".gstarcad .btn").attr("href", currentBit);
	});

	//Switch the download bit options of the GstarCAD
	$("#bitVersion").change(function(){
		var currentBit = $("#bitVersion option:selected").attr("name");
		$(".gstarcad .btn").attr("href", currentBit);
	});

	//Add current attribute to Location Selection Box
	var pathArray = window.location.pathname.split( '/' );
	var partial = pathArray[1];
	$(".select_country option").each(function(){
		if (partial == $(this).attr("id")){
			$(this).attr("selected", true);
		}
	});

	//Home page, #notification window
	$(".window .request").on("click", function(){
		$(this).parents("#notification").addClass("active");
	});

	//Placeholder fix
	/*$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
	    input.val('');
	    input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
	    input.addClass('placeholder');
	    input.val(input.attr('placeholder'));
	  }
	}).blur().parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
	    var input = $(this);
	    if (input.val() == input.attr('placeholder')) {
	      input.val('');
	    }
	  })
	});*/

	//Tooltip
	$(".tooltipable").hover(function(){
		//Take the tooltip content
		var dataTooltipContent = $(this).attr("data-tooltip-content");
		//If tooltip doesn't exist, append the tooltip
		if ($(".tooltip").length == 0){
			$(this).append("<span class='tooltip'>"+dataTooltipContent+"</span>");
		}
	}, function(){
		//Remove the tooltip
		$(".tooltip").remove();
	});
});	

//Add class "Current" to the submenu on scroll
//on the Products pages
function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('#submenu nav li a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top - 83 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#submenu nav li a').removeClass("current");
            currLink.addClass("current");
        }
        else{
            currLink.removeClass("current");
        }
    });
}
