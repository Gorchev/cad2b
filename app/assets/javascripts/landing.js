$(document).ajaxSuccess(function(event, request) {
  var msg = request.getResponseHeader('X-Message');
  if (msg != null) {
  	$("#mailEmail").parent().append("<span class='error'>"+msg+"</span>");
  } else {
  	
  	//Home Page
  	setTimeout(function(){
		$(".home #mailEmail").addClass("hide")
		$(".home span.error").remove()
	}, 200);
	setTimeout(function(){
		$(".home #mailName").addClass("hide")
	}, 400);
	setTimeout(function(){
		$(".home #mailSurname").addClass("hide")
	}, 600);
	setTimeout(function(){
		$(".home .window .send").addClass("hide")
	}, 800);
	setTimeout(function(){
		$(".home .window .send").remove();
		$(".home #mailEmail").remove();
		$(".home #mailName").remove();
		$(".home #mailSurname").remove();
	}, 900);
	setTimeout(function(){
		$(".home .window.second").addClass("move")
		$(".home #notification h2").text("Thank you!")
		$(".home #notification .tagline").text("You should have an email in your inbox!")
	}, 1200);

	//Landing Page
  	$("#mailEmail").parent().find("error").remove();
  	setTimeout(function(){
		$("#leftSection h2").removeClass("moveup");
		$("#leftSection #mailEmail").removeClass("moveup");
	}, 200);
	setTimeout(function(){
		$("#leftSection #mailName").removeClass("moveup");
	}, 300);
	setTimeout(function(){
		$("#leftSection #mailSurname").removeClass("moveup");
	}, 400);
	setTimeout(function(){
		$("#leftSection .send").removeClass("moveup");
	}, 500);
	setTimeout(function(){
		$("#leftSection .share_buttons").removeClass("moveup");
	}, 600);
	setTimeout(function(){
		$("#leftSection .logo").removeClass("moveup");
	}, 700);
	setTimeout(function(){
		$("#leftSection h2").text("Thank you! You should receive an email from us shortly.");
	}, 700);
	setTimeout(function(){
		$("#leftSection h2").addClass("moveup");
	}, 1100);
  };
});

$(document).ready(function(){
	var nameError;
	var surnameError;
	var emailError;
	//Home page, #notification window

	$("#landingPage .send").on("click", function(){
		if(checkName() & checkSurname() & checkMail()){
			return true
		} else {
			return false
		}
	});

	function checkMail(){
		var a = $("#mailEmail").val();
		var regexp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

		if(regexp.test(a)){
			$("#mailEmail").parent().find(".error").remove();
			return true
		} else {
			$("#mailEmail").parent().append("<span class='error'>Please enter valid email</span>");
			return false
		}
	}

	function checkName(){
		if ($("#mailName").val().length < 1){
			$("#mailName").parent().append("<span class='error'>Please enter your name</span>");
			return false
		} else {
			$("#mailName").parent().find(".error").remove();
			return true
		}
	}

	function checkSurname(){
		if ($("#mailSurname").val().length < 1){
			$("#mailSurname").parent().append("<span class='error'>Please enter your surname</span>");
			return false
		} else {
			$("#mailSurname").parent().find(".error").remove();
			return true
		}
	}

	//Parallax
	$(window).on("scroll", function(){
		var distanceFromTop = $(window).scrollTop();
		$("#leftSection").css("background-position-y", (-distanceFromTop/9)+300);
	});

});

window.onload = function(){

	//Features Slider
	function slider(){
		//Width of the main right side container
		var mainWidth = $("#landingPage #rightSection .positioned").width();
		var listEl = $("#landingPage #rightSection .positioned .features li");
		var slideNext = 0;
		var slidePrev = 4;
		//Slider Container
		var sliderCont;

		//List width
		$("#landingPage #rightSection .positioned .features li").width(mainWidth);

		$("#landingPage #rightSection .positioned .features li").each(function(i){
			//Assigning the the sum of all lists to a var
			sliderCont = mainWidth*i+mainWidth;
		});

		//Make the width of the slider container equal to the sum of all slides
		$("#landingPage #rightSection .positioned .features ul").width(sliderCont);

		//Next slide control
		$(".control.next").on("click", function(e){
			//Increment the slide index
			slideNext++
			slidePrev++

			//Start from the beggining of the slider
			if (slideNext > 4){
				slideNext = 0;
				slidePrev = 4;
			}
		});

		//Prev slide control
		$(".control.prev").on("click", function(e){
			//Decrement the slide index
			slidePrev--
			slideNext--

			console.log(slideNext);

			//Start from the end of the slider
			if (slideNext < 0){
				slideNext = 4;
				slidePrev = 3;
			}
		});

		//General slide control
		$(".control").on("click", function(e){
			e.preventDefault();
			
			//Move the slider ul to create the animation
			$("#landingPage #rightSection .positioned .features ul").animate({
				marginLeft: "-"+slideNext*mainWidth
			});
		});

		//Auto slider
		function autoSlide(){

			if ($("#landingPage #rightSection .positioned .features ul").data("state") == "enabled"){
				slideNext++
				slidePrev++
				//Start from the beggining of the slider
				if (slideNext > 4){
					slideNext = 0;
					slidePrev = 4;
				}

				$("#landingPage #rightSection .positioned .features ul").animate({
					marginLeft: "-"+slideNext*mainWidth
				});	
			}

		}

		//Set timer on fot the slide
		var timer = setInterval(autoSlide, 5000);

		//On hover stop the timer, on hover out star the timer
		$("#landingPage #rightSection .positioned .features ul li").hover(function(){
			clearInterval(timer)
		}, function(){
			//nothing
		});	

		//On active video, stop the timer
		$("#landingPage .fake_click").on('click', function(e){
			$(this).next()[0].src += "?autoplay=1";
			clearInterval(timer);
			e.preventDefault();
		});
		/*$("#landingPage #rightSection .positioned li").each(function(video){
			console.log($(this).find(".video iframe")[0]);
		});*/


	}

	$(window).on('resize', function(){
		slider();
		$("#leftSection").width($(window).width()/2);
		$("#leftSection .positioned").css("margin-top", -($("#leftSection .positioned").height()) / 2);
	});

	//Initial animation

	$("#landingPage .loader").addClass("hide");

	setTimeout(function(){
		$("#leftSection .logo-gstar").addClass("active");
		$("#leftSection .logo-gstar").addClass("active");
	}, 300);

	setTimeout(function(){
		$("#leftSection .logo-gstar").addClass("moveup");
	}, 800);

	setTimeout(function(){
		$("#leftSection h1").addClass("active");
	}, 1000);

	//Make the leftSide width half the window
	setTimeout(function(){
		$("#leftSection").width($(window).width()/2);
		//$("#leftSection .positioned").css("max-width", "80%");
	}, 2500);

	//Make the logo and the title to align back to the left
	setTimeout(function(){
		$("#leftSection .logo-gstar").addClass("moveleft");
	}, 2600);
	setTimeout(function(){
		$("#leftSection h1").addClass("moveleft");
	}, 2700);

	//Show the rest of the elements
	setTimeout(function(){
		$("#leftSection .tagline").addClass("moveup");
	}, 2900);
	setTimeout(function(){
		$("#leftSection h2").addClass("moveup");
	}, 3100);
	setTimeout(function(){
		$("#leftSection #mailEmail").addClass("moveup");
	}, 3300);
	setTimeout(function(){
		$("#leftSection #mailName").addClass("moveup");
	}, 3400);
	setTimeout(function(){
		$("#leftSection #mailSurname").addClass("moveup");
	}, 3500);
	setTimeout(function(){
		$("#leftSection .send").addClass("moveup");
	}, 3600);
	setTimeout(function(){
		$("#leftSection .share_buttons").addClass("moveup");
	}, 3700);
	setTimeout(function(){
		$("#leftSection .logo").addClass("moveup");
		$("#leftSection .positioned").css({
			height: "auto",
			top: "50%"
		});
		$("#leftSection .positioned").css("margin-top", -($("#leftSection .positioned").height()) / 2);
	}, 3800);

	//Right side animate
	setTimeout(function(){
		$("#rightSection h2").addClass("show");
	}, 4000);
	setTimeout(function(){
		$("#rightSection .arrow_controls a.prev").addClass("show");
	}, 4100);
	setTimeout(function(){
		$("#rightSection .arrow_controls a.next").addClass("show");
	}, 4200);
	setTimeout(function(){
		slider();
		$("#rightSection .features li .video").addClass("show");
		$(".package.one").addClass("show");
	}, 4300);
	setTimeout(function(){
		$("#rightSection .features li h3").addClass("show");
		$(".package.two").addClass("show");
	}, 4400);
	setTimeout(function(){
		$("#rightSection .features li p").addClass("show");
		$(".package.three").addClass("show");
	}, 4500);
	setTimeout(function(){
		$("#landingPage #rightSection .positioned .more").addClass("show");
		$("#landingPage #versions .starts_from").addClass("show");
	}, 4600);
	setTimeout(function(){
		$("#landingPage #rightSection .positioned .promo").addClass("show");
		$("#landingPage #versions .link_to").addClass("show");
	}, 4700);
}



















