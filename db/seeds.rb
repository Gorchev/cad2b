# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#   Topic.create([{title: 'FAQ for GstarCAD 2016'},{content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel leo ut turpis dictum molestie. Nulla sed viverra nunc. In et dui eleifend, molestie lectus quis, aliquet erat. Fusce vel ultrices diam. Nulla facilisi. Nam eget quam metus. Sed elementum sagittis quam nec dignissim. Nulla interdum eleifend congue. Etiam sodales, nibh at vehicula aliquet, risus massa tincidunt ipsum, eget luctus diam sapien et nisi. Sed id lobortis velit, vitae lacinia ligula. Quisque aliquam turpis et imperdiet sollicitudin."}])
#['registered', 'banned', 'moderator', 'admin'].each do |role|
#	Role.find_or_create_by({name: role})
#end

#User.create([{name: "John", surname: "Doe", email: "john@asas.com", role_id: 1}, {name: "Steve", surname: "Hudelston", email: "john@asas.com", role_id: 1}, {name: "Brand", surname: "Donny", email: "john@asas.com", role_id: 1}, {name: "Mitch", surname: "Hew", email: "john@asas.com", role_id: 1}, {name: "Matt", surname: "McDonalld", email: "john@asas.com", role_id: 1}])
